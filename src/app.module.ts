import { Module } from '@nestjs/common';
import { AppService } from './app.service';
import { DiscordModule } from '@discord-nestjs/core';
import { GatewayIntentBits } from 'discord.js';
import { CommandsModule } from './commands/commands.module';

//bot teste
//MTA4NDg2NTQwNjk5NDQ4NTI4OA.GsB6BI.SgM7cKqxT8XUP_obtt2YrwI_cLxFIbOReCMUnI

@Module({
  imports: [
    DiscordModule.forRootAsync({
      useFactory: () => ({
        token:
          'MTEyNjI3MzAzNjU4NTI4NzY5MA.GtqslU.m0cZWoONG7uaKWZk4OvoOcXTCcV-F0y6GlzQ_c',
        discordClientOptions: {
          intents: [
            GatewayIntentBits.Guilds,
            GatewayIntentBits.GuildMessages,
            // You must allow message content for your application in discord developers
            // https://support-dev.discord.com/hc/en-us/articles/4404772028055
            GatewayIntentBits.MessageContent,
          ],
        },
        prefix: '$',
      }),
    }),
    CommandsModule,
  ],
  providers: [AppService],
})
export class AppModule {}
