import { Injectable } from '@nestjs/common';
import { InjectDiscordClient } from '@discord-nestjs/core';
import { Client } from 'discord.js';
type User = { id: string; username: string; amount: number };

import * as fs from 'fs';
import { CommandsService } from './commands/commands.service';

@Injectable()
export class AppService {
  dataBasePath = 'src/data/database.json';

  constructor(
    @InjectDiscordClient()
    private readonly client: Client,
    private service: CommandsService,
  ) {
    this.spyInteraction();
    this.spyInteractionMessage();
    this.service.updateChannelId();
  }

  spyInteraction() {
    this.client.on('interactionCreate', async (item) => {
      if (!item.isButton()) return;

      if (item.customId === 'resetAmount')
        await this.resetUserAmmount(this.extrairID(item.message.content));

      if (item.customId === 'plus')
        await this.plusUserAmmount(this.extrairID(item.message.content));

      if (item.customId === 'minus')
        await this.minusUserAmmount(this.extrairID(item.message.content));

      await item.deferUpdate({ ephemeral: true } as any);
    });
  }

  async spyInteractionMessage() {
    this.client.on('messageCreate', async (item) => {
      if (item.reference && item.mentions?.repliedUser?.bot) {
        try {
          const referencedMessage = await item.channel.messages.fetch(
            item.reference['messageId'],
          );

          if (referencedMessage.interaction.user.id === item.author.id) {
            await referencedMessage.edit(item.content);

            await item.delete();
          } else {
            item.reply('Essa mensagem não pode ser editada pq não é sua');
          }
        } catch (error) {
          console.error('Erro ao buscar a mensagem original:', error);
        }
      }
    });
  }

  async readJson(path: string) {
    const data = await fs.promises.readFile(path, 'utf-8');

    return JSON.parse(data);
  }

  extrairID(string): string {
    const regex = /id:\s*(\d+)/i;
    const match = string.match(regex);
    if (match && match[1]) {
      return match[1];
    }
    return null;
  }

  async resetUserAmmount(id: string) {
    const data = (await this.readJson(this.dataBasePath)) as User[];
    const index = data.findIndex((item) => item.id === id);
    data[index].amount = 0;

    await this.service.updateControllerChannel(id, data);
    await fs.promises.writeFile(this.dataBasePath, JSON.stringify(data));
  }

  async plusUserAmmount(id: string) {
    const data = (await this.readJson(this.dataBasePath)) as User[];
    const index = data.findIndex((item) => item.id === id);
    data[index].amount = data[index].amount + 1;

    await this.service.updateControllerChannel(id, data);
    await fs.promises.writeFile(this.dataBasePath, JSON.stringify(data));
  }

  async minusUserAmmount(id: string) {
    const data = (await this.readJson(this.dataBasePath)) as User[];
    const index = data.findIndex((item) => item.id === id);
    if (data[index].amount >= 1) data[index].amount = data[index].amount - 1;

    await this.service.updateControllerChannel(id, data);
    await fs.promises.writeFile(this.dataBasePath, JSON.stringify(data));
  }
}
