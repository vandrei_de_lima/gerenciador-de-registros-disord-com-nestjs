import { Injectable } from '@nestjs/common';
import { SlashCommandPipe } from '@discord-nestjs/common';
import {
  Command,
  EventParams,
  Handler,
  InteractionEvent,
} from '@discord-nestjs/core';
import { ClientEvents, PermissionFlagsBits } from 'discord.js';
import { RegistrationDto } from './dtos/register.dto';
import { CommandsService } from './commands.service';

@Command({
  name: 'registrar',
  description: 'Registrar treinamento',
  defaultMemberPermissions: PermissionFlagsBits.SendMessages,
})
@Injectable()
export class RegisterCommand {
  constructor(private service: CommandsService) {}

  @Handler()
  onResguster(
    @InteractionEvent(SlashCommandPipe) dto: RegistrationDto,
    @EventParams() args: ClientEvents['interactionCreate'],
  ): string {
    this.service.registerTraining(args[0] as any);

    const split = dto.register.split(',');

    return split.join('\n');
  }
}
