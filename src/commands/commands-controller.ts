import { Injectable } from '@nestjs/common';
import { Command, Handler } from '@discord-nestjs/core';
import { CommandInteraction } from 'discord.js';
import { CommandsService } from './commands.service';

@Command({
  name: 'controlador',
  description: 'Configurar local de set reset dos treinamentos',
})
@Injectable()
export class ControllerCommand {
  constructor(private service: CommandsService) {}

  @Handler()
  onResguster(interaction: CommandInteraction): string {
    this.service.registerControllerChannel(interaction as any);

    return 'Canal controlador registrado';
  }
}
