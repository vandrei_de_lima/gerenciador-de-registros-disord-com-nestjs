import { CommandInteraction, Message } from 'discord.js';
import { Injectable } from '@nestjs/common';
import { InjectDiscordClient } from '@discord-nestjs/core';
import {
  Client,
  ButtonBuilder,
  ButtonStyle,
  ActionRowBuilder,
} from 'discord.js';
type User = { id: string; username: string; amount: number; nickname: string };

import * as fs from 'fs';

@Injectable()
export class CommandsService {
  dataBasePath = 'src/data/database.json';
  configPath = 'src/data/config.json';
  channelId = '';

  tax = '25';

  constructor(
    @InjectDiscordClient()
    private readonly client: Client,
  ) {}

  async updateChannelId() {
    const config = await fs.promises.readFile(this.configPath, 'utf-8');
    const configJSON = JSON.parse(config);
    this.channelId = configJSON.channelIdController || '';
    this.tax = configJSON.tax || '25';
  }

  async readJson(path: string) {
    const data = await fs.promises.readFile(path, 'utf-8');

    return JSON.parse(data);
  }

  async registerTraining(interaction: CommandInteraction) {
    await this.updateChannelId();

    const objetoJSON = (await this.readJson(this.dataBasePath)) as User[];

    const { username, id } = interaction.user;
    const nickname = (interaction as any).member.nickname;

    const isSaved = objetoJSON.some((item) => item.id == id);
    const index = objetoJSON.findIndex((item) => item.id === id);

    if (isSaved && !objetoJSON[index]?.nickname) {
      objetoJSON[index].nickname = nickname;
    }

    if (!isSaved) {
      const user = { nickname, username, id, amount: 1 };
      objetoJSON.push(user);
      this.createRegisterTrainers(id, objetoJSON).then();
    } else {
      this.updateRegisterTrainers(id, objetoJSON).then();
    }
  }

  async createRegisterTrainers(id, objetoJSON) {
    await fs.promises.writeFile(this.dataBasePath, JSON.stringify(objetoJSON));
    this.updateControllerChannel(id, objetoJSON);
  }

  async updateRegisterTrainers(id: string, objetoJSON) {
    try {
      const index = objetoJSON.findIndex((item) => item.id === id);

      objetoJSON[index].amount = objetoJSON[index].amount + 1;

      await fs.promises.writeFile(
        this.dataBasePath,
        JSON.stringify(objetoJSON),
      );
      this.updateControllerChannel(id, objetoJSON);
      console.log('Arquivo JSON salvo com sucesso!');
    } catch (error) {
      console.error('Erro ao salvar o arquivo JSON:', error);
    }
  }

  async registerControllerChannel(interaction: CommandInteraction) {
    this.channelId = interaction.channelId;

    const objetoJSON = await this.readJson(this.configPath);

    objetoJSON.channelId = this.channelId;

    await fs.promises.writeFile(this.configPath, JSON.stringify(objetoJSON));
  }

  async updateTax(value: string) {
    this.tax = value;

    const objetoJSON = await this.readJson(this.configPath);

    objetoJSON.tax = this.tax;

    await fs.promises.writeFile(this.configPath, JSON.stringify(objetoJSON));
  }

  async updateControllerChannel(id: string, objetoJSON, reload = false) {
    const index = objetoJSON.findIndex((item) => item.id === id);
    const user = objetoJSON[index];
    const channel = await this.client.channels.fetch(this.channelId);
    const confirm = new ButtonBuilder()
      .setCustomId('resetAmount')
      .setLabel('Resetar contador')
      .setStyle(ButtonStyle.Primary);

    const plus = new ButtonBuilder()
      .setCustomId('plus')
      .setLabel('+')
      .setStyle(ButtonStyle.Success);

    const minus = new ButtonBuilder()
      .setCustomId('minus')
      .setLabel('-')
      .setStyle(ButtonStyle.Danger);

    const row = new ActionRowBuilder().addComponents(confirm, minus, plus);

    channel['messages'].fetch({ limit: 100 }).then((messagesPage) => {
      let messagesBot: Message[] = [];
      const messages = [];
      messagesPage.forEach((msg) => messages.push(msg));

      messagesBot = messages.filter((message: Message) => message.author.bot);

      if (messagesBot.length) {
        const index = messagesBot.findIndex((message: Message) =>
          message.content.includes(`${id}`),
        );

        if (index !== -1 && !reload) {
          const msg = {
            content: this.generateMessage(user),
          };

          const messageUser: Message = messagesBot[index];
          messageUser.edit(msg);
        } else {
          if (index !== -1) {
            const messageUser: Message = messagesBot[index];

            messageUser.delete();
          }

          const msg = {
            content: this.generateMessage(user),
            components: [row],
          };

          channel['send'](msg);
        }
      } else {
        const msg = {
          content: this.generateMessage(user),
          components: [row],
        };
        channel['send'](msg);
      }
    });
  }

  generateMessage(user: User) {
    return `\`\`\`id: ${user.id} \n | Treinador: ${
      user?.nickname || user.username
    } \n | Quantidade de treino: ${user.amount} \n | Total: $${
      user.amount * parseInt(this.tax)
    }\`\`\``;
  }

  async reloadController() {
    const objetoJSON = (await this.readJson(this.dataBasePath)) as User[];

    objetoJSON.forEach((item) => {
      this.updateControllerChannel(item.id, objetoJSON, true);
    });
  }
}
