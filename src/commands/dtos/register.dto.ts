import { Param } from '@discord-nestjs/core';

export class RegistrationDto {
  @Param({
    name: 'register',
    description: 'Registro com os dados do treinamento',
    required: true,
  })
  register: string;
}
