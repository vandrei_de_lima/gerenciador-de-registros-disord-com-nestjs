import { Param } from '@discord-nestjs/core';

export class TaxDto {
  @Param({
    name: 'taxa',
    description: 'Setar o valor da taxa',
    required: true,
  })
  valor: string;
}
