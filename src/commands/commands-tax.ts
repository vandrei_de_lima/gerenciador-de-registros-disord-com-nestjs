import { Injectable } from '@nestjs/common';
import {
  Command,
  EventParams,
  Handler,
  InteractionEvent,
} from '@discord-nestjs/core';
import { ClientEvents } from 'discord.js';
import { CommandsService } from './commands.service';
import { TaxDto } from './dtos/tax.dto';
import { SlashCommandPipe } from '@discord-nestjs/common';

@Command({
  name: 'taxa',
  description: 'Setar o valor da taxa',
})
@Injectable()
export class TaxCommand {
  constructor(private service: CommandsService) {}

  @Handler()
  onResguster(
    @InteractionEvent(SlashCommandPipe) dto: TaxDto,
    @EventParams() args: ClientEvents['interactionCreate'],
  ): string {
    if (!isNaN(parseInt(dto.valor))) {
      this.service.updateTax(dto.valor);

      return 'Taxa atualizada, as novas mensagens viram com a taxa atualizada.';
    } else {
      return 'Valor invalido';
    }
  }
}
