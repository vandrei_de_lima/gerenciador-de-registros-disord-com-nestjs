import { DiscordModule } from '@discord-nestjs/core';
import { Module } from '@nestjs/common';
import { RegisterCommand } from './commands-register';
import { CommandsService } from './commands.service';
import { ControllerCommand } from './commands-controller';
import { ReloadControllerCommand } from './commands-reset-controller';
import { TaxCommand } from './commands-tax';

@Module({
  imports: [DiscordModule.forFeature()],
  providers: [
    RegisterCommand,
    ControllerCommand,
    ReloadControllerCommand,
    CommandsService,
    TaxCommand,
  ],
  exports: [CommandsService],
})
export class CommandsModule {}
