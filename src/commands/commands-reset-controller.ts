import { Injectable } from '@nestjs/common';
import { Command, Handler } from '@discord-nestjs/core';
import { CommandInteraction } from 'discord.js';
import { CommandsService } from './commands.service';

@Command({
  name: 'recarregar-controlador',
  description: 'Limpa e lista novamente todos os treinadores',
})
@Injectable()
export class ReloadControllerCommand {
  constructor(private service: CommandsService) {}

  @Handler()
  onResguster(interaction: CommandInteraction): string {
    this.service.reloadController();

    return 'Recarregado';
  }
}
